cmake_minimum_required(VERSION 3.16)

# Set the project name and language
project(
  template
  VERSION 0.0.1
  LANGUAGES CXX C)

include(cmake/Cache.cmake)
include(cmake/CompilerWarnings.cmake)
include(cmake/PreventInSourceBuilds.cmake)
include(cmake/StandardProjectSettings.cmake)
include(cmake/Standards.cmake)
enable_cache()
set_standards()

# Create interface for project options
add_library(project_options INTERFACE)
target_compile_features(project_options INTERFACE cxx_std_${CMAKE_CXX_STANDARD})
target_include_directories(project_options INTERFACE .)

# Create interface to use the warnings specified in CompilerWarnings.cmake
set(WARNINGS_AS_ERRORS ON)
add_library(project_warnings INTERFACE)
set_project_warnings(
  project_warnings
  ${WARNINGS_AS_ERRORS}
)

add_subdirectory(template)
add_subdirectory(tests)